---
title: How To Use
---

1. Create a hugo markdown folder in your repository. The folder name should be 'content'
2. Paste the following code inside .gitlab-ci.yml:

```yaml
image: ubuntu:18.04
```

```yaml
stages:
 - page-generation
```

```yaml
pages:
    stage: page-generation
    script:
        - apt-get update && apt-get install -y git && git clone https://gitlab.com/j-tech/website-builder.git
        - website-builder/build_site.sh ./content ./public
    artifacts:
        paths:
            - public
    only:
        - master
```

3. Upload to gitlab
