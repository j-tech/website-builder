---
title: Website Builder
---

# Website Builder

The Website Builder is a helper script used by J-Tech to generate websites using Hugo and Gitlab pages. 
This website functions as a test bed as well as documentation for the script. In other words, if you can see this site, then the script works. 

The main repository is here: https://gitlab.com/j-tech/website-builder
