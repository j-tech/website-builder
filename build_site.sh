#!/bin/sh

CONTENT_DIR=$(realpath $1);
OUTPUT_DIR=$(realpath $2);

SCRIPT_FOLDER=$(realpath $(dirname $0));
apt-get -qq update && apt-get install -y git wget curl;
git clone https://github.com/matcornic/hugo-theme-learn.git $SCRIPT_FOLDER/themes/hugo-theme-learn;
$SCRIPT_FOLDER/get_latest_hugo.sh;
hugo -s $SCRIPT_FOLDER -c $CONTENT_DIR -b $CI_PAGES_URL -d $OUTPUT_DIR;
